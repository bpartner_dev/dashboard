<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Dashboard title
     |--------------------------------------------------------------------------
     */
    'title' => 'WebMagic Dashboard',

    /*
     |--------------------------------------------------------------------------
     | Dashboard logo configuration
     |--------------------------------------------------------------------------
     */
    'logo' => [
        'link' => '/dashboard',
        'icon' => '/webmagic/dashboard/img/default_logo.png',
        'text' => 'WebMagic'
    ],

    /*
     |--------------------------------------------------------------------------
     | Admin panel style
     | docs url /dashboard/tech/change-admin-panel-style-description
     |--------------------------------------------------------------------------
     */

    'admin_panel_style' => [

        'show_user_panel' => true,

        'body' => [
            'small_text' => false,
            'accent_color_variants' => '',
        ],

        'nav_bar' => [
            'no_navbar_border' => false,
            'small_text' => false,
            'variants' => [
                'font_color' => 'dark',
                'theme_color' => 'white'
            ],
        ],

        'side_bar' => [
            'small_text' => false,
            'flat' => false,
            'legacy' => false,
            'compact' => false,
            'child_indent' => true,
            'collapsed' => false
        ],

        'footer' => [
            'small_text' => false,
        ],

        'aside' => [
            'auto_expand' => true,
            'variants' => [
                'theme_color' => 'dark',
                'background_color' => 'primary'
            ],
        ],

        'logo' => [
            'small_text' => false,
            'color' => 'gray-dark',
        ],
    ],

    /*
     |--------------------------------------------------------------------------
     | Menu configuration
     |--------------------------------------------------------------------------
     */
    'menu' => [],

    /*
     |--------------------------------------------------------------------------
     | NavBar menu
     |--------------------------------------------------------------------------
     */
    'header_navigation' => [
        [
            'text' => 'Logout',
            'icon' => '',
            'link' => 'logout',
            'class' => 'nav-item'
        ],
        [
            'text' => 'Preview site',
            'icon' => '',
            'link' => '/',
            'class' => 'nav-item',
            'target' => '_blank'
        ]
    ],
    /** Navbar options */
    'header_navigation_options' => [
        'show_collapse_sidebar_btn' => true,
        'show_home_btn' => true,
        'show_fullscreen_btn' => true
    ],

    /*
     |--------------------------------------------------------------------------
     | Default image for preview
     |--------------------------------------------------------------------------
     |
     | You can see the available dashboard components if enabled
     |
     */
    'default_image' => '/webmagic/dashboard/img/default-image-png.png',

    /*
     |--------------------------------------------------------------------------
     | Activate dashboard presentation mode
     |--------------------------------------------------------------------------
     |
     | You can see the available dashboard components if enabled
     |
     */
    'presentation_mode' => true,

    /*
     |--------------------------------------------------------------------------
     | Available types of notifications & their icons
     |--------------------------------------------------------------------------
     */
    'available_notification_types' => [
        'info' => 'info',
        'danger' => 'ban',
        'warning' => 'warning',
        'success' => 'check'
    ],

    /*
     |--------------------------------------------------------------------------
     | Dashboard API
     |--------------------------------------------------------------------------
     */
    'api_middleware' => ['web', 'auth'],

    /*
     |--------------------------------------------------------------------------
     | Images storage path
     |--------------------------------------------------------------------------
     | Path is related to the public storage directory /storage/app/public/...
     | It available from the front and as /storage/...
     | Public storage symlink should be generated first
     |
     */
    'images_directory' => 'dashboard/images',
];
