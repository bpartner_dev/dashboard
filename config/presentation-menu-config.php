<?php

return [
    [
        'text'     => 'Dashboard details',
        'icon'     => 'fa-info',
        'subitems' => [
            [
                'link'         => 'dashboard/tech/installation',
                'text'         => 'Installation',
                'icon'         => 'far fa-circle',
                'active_rules' => [
                    'urls' => [
                        'dashboard/tech/installation'
                    ],
                ],
            ],
            [
                'link'         => 'dashboard/tech/change-admin-panel-style-description',
                'text'         => 'Change style',
                'icon'         => 'far fa-circle',
                'active_rules' => [
                    'urls' => [
                        'dashboard/tech/change-admin-panel-style-description'
                    ],
                ],
            ],
            [
                'text'     => 'Pages',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/tiles-list-page-description',
                        'text'         => 'Tiles list page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/tiles-list-page',
                                'dashboard/tech/tiles-list-page-description',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/form-page',
                        'text'         => 'Form Page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/form-page',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/login-page',
                        'text'         => 'Login Page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/login-page',
                        ],
                    ],
                ],
            ],
            [
                'text'     => 'Table',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/tables/manual-sorting',
                        'text'         => 'Manual sorting',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/tables/manual-sorting',
                                'dashboard/tech/tables/manual-sorting-example',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/table-page-description',
                        'text'         => 'Table Page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/table-page',
                                'dashboard/tech/table-page-description',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/tables/pagination',
                        'text'         => 'Pagination',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/tables/pagination',
                                'dashboard/tech/tables/pagination/base-using',
                                'dashboard/tech/tables/pagination/base-using-with-dropdown',
                                'dashboard/tech/tables/pagination/with-custom-per-page',
                                'dashboard/tech/tables/pagination/with-two-tables',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'text'     => 'Elements',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/main-menu',
                        'text'         => 'Main menu',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/main-menu',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/photo-uploading',
                        'text'         => 'Photo uploading',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/photo-uploading',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/date-dropdown',
                        'text'         => 'Date dropdown',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/date-dropdown',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/images',
                        'text'         => 'Images',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/images',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/visual-editor',
                        'text'         => 'Visual Editor',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/visual-editor',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/breadcrumbs',
                        'text'         => 'Breadcrumbs',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/breadcrumbs',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/auto-update',
                        'text'         => 'Auto update elements',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/auto-update',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/notifications-description',
                        'text'         => 'Notifications',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/notifications-description',
                                'dashboard/tech/notifications',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/date-range',
                        'text'         => 'Date range',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/date-range',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/date-range-js',
                        'text'         => 'Date range js',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/date-range-js',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/box',
                        'text'         => 'Box',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/box',
                        ],
                    ],
                ],
            ],
            [
                'text'     => 'JS actions',
                'icon'     => 'fa-circle',
                'subitems' =>
                    [
                        [
                            'text'         => 'Fast JS Actions',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions',
                            ],
                        ],
                        [
                            'text'         => 'Mask',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/mask-description',
                            'active_rules' => [
                                'urls' => [
                                    'dashboard/tech/mask',
                                    'dashboard/tech/mask-description',
                                ],
                            ],
                        ],
                        [
                            'text'         => 'Confirmation popup',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/confirmation-popup',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/confirmation-popup',
                            ],
                        ],
                        [
                            'text'         => 'Tooltips',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/tooltips',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/tooltips',
                            ],
                        ],
                        [
                            'text'         => 'Content copy',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/content-copy-to-clipboard',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/content-copy-to-clipboard',
                            ],
                        ],
                    ],

            ],
            [
                'text'     => 'JS scripts',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/js-scripts/sortable',
                        'text'         => 'Sortable',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/sortable',
                        ],
                    ],
                ]
            ]
        ],
    ],
];
