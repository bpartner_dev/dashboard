## Box
``` \Webmagic\Dashboard\Elements\Boxes\Box```

For simple card creation

### How to use

```php
(new Box())
    ->addBoxType()
    ->addBoxTitle('<b>This form can be expanded to full screen</b>')
    ->addBoxTools()
    ->addBoxBody([
        'content1',
        (new TableGenerator())->items($items),
    ])
    ->addBoxFooter('Footer')
    ->addClass(true)
    ->addBoxBodyClasses('')
    ->addBoxHeaderContent('')
    ->addHeaderAvailable(true) // $header_available = true
    ->addFooterAvailable(true) // $footer_available = true
    ->addColorType('default')  // $color_type = default [default/primary/info/warning/success/danger]
    ->addSolid(true)           // $solid = false
    ->showFullscreenBtn();      // $fullscreen_btn = false
```  
