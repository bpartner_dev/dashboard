## Date range js

You can enable users to quickly and easily set the desired date range

Here is an example usage in FormPageGenerator:

```php
    (new FormPageGenerator())
            ->title('Page title', 'Page sub-title')
            ->method('POST')
            ->action('/')
            ->ajax(true)
            ->datePickerJS(
                'date_js',      // $name
                today(),        // valueOrDataSource = null
                'Select date',  // $labelTxt = ''
                true,           // $required = false
                [],             // $attributes = []
                false           // $defaultRanges = false
            )
            ->dateTimePickerJS(
                'date_time_js', // $name
                now(),          // $valueOrDataSource = null
                'Select date',  // $labelTxt = ''
                false,          // $required = false
                [],             // $attributes = []
                false,          // $time24Format = true
                false,          // showSeconds = true
                true            // $defaultRanges = false
            )
            ->dateRangePicker(
                'date_range_start',       // $name
                'date_range_end',         // $endName
                today(),                  // $valueOrDataSource = null
                today(),                  // $endValueOrDataSource = null
                'Select range of dates',  // $labelTxt = ''
                true,                     // $required = false
                true,                     // $endRequired = false
                [],                       // $attributes = []
                true,                     // $time24Format = true
                true,                     // $showSeconds = true
                true                      // $defaultRanges = false
            )
            ->dateTimeRangePicker(
                'date_time_range_start',  // $name
                'date_time_range_end',    // $endName
                now(),                    // $valueOrDataSource = null
                now(),                    // $endValueOrDataSource = null
                'Select range of dates and times', // $labelTxt = ''
                false,                    // $required = false
                false,                    // $endRequired = false
                [],                       // $attributes = []
                true,                     // $time24Format = true
                true                      // $showSeconds = true
             );
```