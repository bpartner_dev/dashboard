## Date range


You can enable users to quickly and easily set the desired date range

Here is an example usage in FormGroup
```php
    (new FormGroup())->element()->dateRange()->name('selectName')
```
The form returns data for a period in the "day.month.year-day.month.year" format. All dates with leading zeros. The year is a two-digit number.

Usage in FormPageGenerator:
```php
    (new FormPageGenerator())->dateRange('date_range')
```
Example:
when you select the current quarter and send the form, the line "01.01.21-30.03.21" (today 17.02.21) will be sent to the server.

### Custom

Also, you can clear the default dataset and add your own date ranges:
```php
    (new FormGroup())
        ->element()
        ->dateRange()
        ->clearRanges()
        ->addRange(
            'custom range', 
            Carbon::now()->subDays(5), 
            Carbon::yesterday()
        )
    ;
```