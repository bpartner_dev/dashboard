# Form page generation

You have two ways to generate the form. Using the `FormPageGenerator` class or using the `FormGroup`.
This demonstrates how the FormPageGenerator and FormGroup classes work.
## FormGroup
```php
$formPageGenerator = (new FormPageGenerator())
    ->title('Creating new movement from account to account ')
    ->action(route('/'));

    $formPageGenerator->getBox()->element()->grid([
        (new Box())
            ->boxHeaderContent('From')
            ->content([
                (new FormGroup())->select('outgoing_account_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Account', true),
                (new FormGroup())->dateInput('outgoing_date', now(), 'Date', true),
                (new FormGroup())->numberInput('value', 0, 'Value', true, 0.01),
                (new FormGroup())->numberInput('outgoing_commission', 0, 'Commission', false, 0.01),
                (new FormGroup())->select('outgoing_contractor_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Contractor'),
            ]),
        (new Box())
            ->boxHeaderContent('&nbsp')
            ->content([
                (new FormGroup())->numberInput('rate', 1, 'Rate', true, 0.01),
                (new FormGroup())->textarea('comment', '', 'Comment', false, ['rows' => 8]),
            ]),
        (new Box())
            ->boxHeaderContent('<b>To</b>')
            ->content([
                (new FormGroup())->select('incoming_account_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Account', true),
                (new FormGroup())->datePickerJS('incoming_date', now(), 'Date', true),
                (new FormGroup())->numberInput('value', 0, 'Value', true, 0.01),
                (new FormGroup())->numberInput('incoming_commission', 0, 'Commission', false, 0.01),
                (new FormGroup())->select('incoming_contractor_id', array_prepend([1 => 'Option 1', 2 => 'Option 2'], '', ''), '', 'Contractor'),
            ]),
        ])->lgRowCount(3);

    $formPageGenerator->submitButtonTitle('Make transaction and go to it');
```
## FormGenerator

```php
$img = 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';    

$formPageGenerator = (new \Webmagic\Dashboard\Components\FormPageGenerator())
    ->title('Page title', 'Page sub-title')
    ->method('POST')    // default POST
    ->action('/')        // default '/'
    ->ajax(true)         // set form to send with Ajax. Default 'true'
    ->input('test_name', 'Submit with custom input', '', 'submit', false, '', [], 'btn btn-default')
    ->hiddenInput('hidden_attribute', null)
    ->textInput('name', null, 'Name', true)
    ->slugInput('slug', 'name', null, 'Slug generated automatically based on name', false, '-', 'lowercase')
    ->numberInput('number', 0, 'Number input', false, 0.01)
    ->emailInput('email', 'tesdt@email.com', 'Email', true)
    ->passwordInput('password', '123', 'Password', true)
    ->colorInput('color', '#000000', 'Select color', false)
    ->checkbox('checkbox_name', false, 'Check me')
    ->switcher('switcher_name', true, 'Switch me')
    // Regular date input
    ->dateInput('date', today(), 'Date', true)
    // Date picker JS
    ->datePickerJS('date_js', today(), 'Select date with JS', true, [], true)
    // Date range picker
    ->dateRangePicker('date_range_start', 'date_range_end', today(), today(), 'Select range of dates', true, true, true)
    // Regular time picker
    ->timeInput('time', now(), 'Time')
    // Time picker JS
    ->timePickerJS('time_js', now(), 'Select time with JS', false, [], true, true)
    // Regular date and time input
    ->dateTimeInput('date_time', now(), 'DateTime', false)
    // Date and time picker with JS
    ->dateTimePickerJS('date_time_js', now(), 'Select date and time with JS (12h-format without seconds)', false, [], false, false, true)
    // Date and time range picker
    ->dateTimeRangePicker('date_time_range_start', 'date_time_range_end', today(), today(),
        'Select range of dates and times', false, false, [], true, true, true)
    // Date range
    ->dateRange('date_range', 'Date range')
    // Regular select
    ->select('select', [1 => 'Option 1', 2 => 'Option 2'], 2, 'Select me', false)
    // Multiply select
    ->select('select', [1 => 'Option 1', 2 => 'Option 2'], 2, 'Select me twice', false, true)
    // Regular JS select
    ->selectJS('select', [1 => 'Option 1', 2 => 'Option 2'], 2, 'Select me with JS', false)
    // JS Select with autocomplete on back-end
    ->selectWithAutocomplete('select', route('dashboard.docs.presentation.select-autocomplete'), [1 => 'London', 5 => 'Paris'], 1, 'Search with back-end autocomplete', false, true)
    ->textarea('comment', '', 'Comment')
    ->visualEditor('content', '<p>test</p>', 'Editor', true, ['disabled' => true]) // Additional params can turn on image uploading functionality
    ->fileInput('file', request(), 'File')
    ->imageInput('testImag', $img, 'Image block', '20 Mb', '10', '234', $img, 'myImage.png')
    ->submitButtonTitle('Push me')
    // Add additional button to submit the form with additional params which will be send to backend
    ->addSubmitButton(['redirect' => url('dashboard')], 'Submit and back to dashboard')
    // Add additional link button
    ->addLinkButton(url('/'), 'Go home');

return $formPageGenerator;
```

If you need to return only form on AJAX request you DON'T NEED to use expression:
```php
if(request()->ajax()){
    return $formPageGenerator->getForm();
}
```
If you need to return the ***entire page*** during an AJAX request, you SHOULD use the **renderNative** function:
```php
if(request()->ajax()){
    return $formPageGenerator->renderNative();
}
```