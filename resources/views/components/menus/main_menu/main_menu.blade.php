@if (Auth::check() && $showUserPanel)
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <div class="userFirstLetter">{{ $userFirstLetter }}</div>
        </div>
        <div class="info user-panel-name">
            <span class="d-block">{{ $userTitle }}</span>
        </div>
    </div>
@endif
@if(!Auth::check())
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
            <a href="{{route('login')}}" class="d-block">Sign in</a>
        </div>
    </div>
@endif

<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column {{ $class }}" data-widget="treeview" role="menu" data-accordion="false">
        {!! $content !!}
    </ul>
</nav>
