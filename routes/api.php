<?php

Route::group([
    'prefix'     => 'dashboard/api',
    'as'         => 'dashboard.api.',
    'namespace'  => 'Webmagic\Dashboard\Api\Http',
    'middleware' => config('webmagic.dashboard.dashboard.api_middleware'),
], function () {
    /**
     * Images API
     */
    Route::post('image/{field_name}', [
       'as' => 'image.upload',
       'uses' => 'ImagesController@upload'
    ]);

    Route::delete('image', [
        'as' => 'image.delete',
        'uses' => 'ImagesController@delete'
    ]);

    /**
     * Dashboard settings API
     */
    Route::post('settings/dark-mode-toggle', function(){
        $key = 'dashboard.settings.dark-mode';
        $currentMode = session($key, false);

        // Toggle value and save in session
        session()->put('dashboard.settings.dark-mode', $currentMode ^= 1);

        return session($key, false);
    });
    
});
