<?php

// Date Dropdown element
Route::get('date-dropdown', [
    'as'   => 'date-dropdown',
    'uses' => 'PresentationController@dateDropdown',
]);

// Date Dropdown element
Route::get('images', [
    'as'   => 'images',
    'uses' => 'PresentationController@imageDisplaying',
]);

// Autoupdate element
Route::get('auto-update', [
    'as'   => 'auto-update',
    'uses' => 'PresentationController@autoUpdate',
]);

// Autocomplete element
Route::get('select-autocomplete', [
    'as'   => 'select-autocomplete',
    'uses' => 'PresentationController@autoComplete',
]);

//BreadCrumbs
Route::get('breadcrumbs', [
    'as'   => 'breadcrumbs',
    'uses' => 'PresentationController@breadcrumbs',
]);

// Photos uploading plugin
Route::match(['get', 'post', 'delete'], 'photo-uploading', [
    'as'   => 'photo-uploading',
    'uses' => 'PresentationController@photoUploading',
]);

// Main menu
Route::get('main-menu', [
    'as'   => 'main-menu',
    'uses' => 'PresentationController@mainMenu',
]);

// Visual editor
Route::get('visual-editor', [
    'as'   => 'visual-editor',
    'uses' => 'PresentationController@visualEditor',
]);

// Box
Route::get('box', [
    'as'   => 'box',
    'uses' => 'PresentationController@box',
]);
