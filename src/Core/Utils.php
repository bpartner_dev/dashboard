<?php


namespace Webmagic\Dashboard\Core;


class Utils
{
    /**
     * Appends given parameters to URL
     *
     * @param string $url
     * @param array  $params
     *
     * @return string
     */
    static public function appendsParamsToURL(string $url, array $params): string
    {
        //Need to generate query first, because some of params can has no values
        $query = http_build_query($params);
        if($query == ''){
            return $url;
        }

        //Add to existing params if needed
        // Need to check if $url is empty because of the 'strpos' error
        if($url != '' && strpos('?', $url) != false){
            return "$url&$query";
        }

        return "$url?$query";
    }

    /**
     * Appends all parameters from request to URL
     *
     * @param string $url
     * @param array  $excludedKeys
     *
     * @return string
     */
    static public function appendsAllRequestToURL(string $url, array $excludedKeys = []): string
    {
        return self::appendsParamsToURL($url, request()->except($excludedKeys));
    }
}
