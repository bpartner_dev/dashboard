<?php

namespace Webmagic\Dashboard\Docs\Http;

use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class DateRangeJsController
{
    /**
     * Tooltips functionality presentation
     *
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function example() : Dashboard
    {
        $dashboard = new Dashboard();

        $content = view()->file(__DIR__ . '/../../../docs/elements/date-range-js.md');

        $formPageGenerator = (new FormPageGenerator())
            ->method('POST')
            ->action('/')
            ->ajax(true)
            ->datePickerJS('date_js', today(),
                '(datePickerJS) Select date',
                true, [])

            ->dateTimePickerJS('date_time_js_1', now(),
                '(dateTimePickerJS) Select date and time',
                false, [], false, false)

            ->dateTimePickerJS('date_time_js_2', now(),
                '(dateTimePickerJS) Select date and time with 24 format',
                false, [], true, false)

            ->dateTimePickerJS('date_time_js_3', now(),
                '(dateTimePickerJS) Select date and time with default ranges',
                false, [], false, false)

            ->dateRangePicker('date_range_start', 'date_range_end', today(), today(),
                '(dateRangePicker) Select range of dates',
                true, true, [], true)

            ->dateTimeRangePicker('date_time_range_start_1', 'date_time_range_end', now(), now(),
                '(dateTimeRangePicker) Select range of dates and times',
                false, false, [], true, true)

            ->dateTimeRangePicker('date_time_range_start_2', 'date_time_range_end', now(), now(),
                '(dateTimeRangePicker) Select range of dates and times with default ranges',
                false, false, [], true, true, true);

        $formPageGenerator->getForm()->sendAllCheckbox(true);

        $dashboard->page()
            ->setPageTitle('')
            ->addElement()->tabs()->addTab()->title('Description')->content($content)->active()
            ->parent()->addTab()->title('Example')->content($formPageGenerator->getBox());

        return $dashboard;
    }
}
