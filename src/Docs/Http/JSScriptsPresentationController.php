<?php


namespace Webmagic\Dashboard\Docs\Http;


use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class JSScriptsPresentationController
{
    /**
     * Tooltips functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function sortable(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-scripts/sortable.md');

        $dashboard->page()
            ->setPageTitle('Sortable JS script')
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }
}